<?php

/* Solucao paliativa para acesso as APIs dos Correios dentro do Pedro Teixeira
A.Letti - 10/9/23 - letti.com.br
*/

define('ENDP_API', 'https://api.correios.com.br/token/v1/autentica/cartaopostagem');
define('ENDP_API_PRAZO_H', 'https://apihom.correios.com.br/prazo/v1/nacional/');
define('ENDP_API_PRAZO', 'https://api.correios.com.br/prazo/v1/nacional/');

define('ENDP_API_PRECO_H', 'https://apihom.correios.com.br/preco/v1/nacional/');
define('ENDP_API_PRECO', 'https://api.correios.com.br/preco/v1/nacional/');

function getToken($access_code, $cartao)
{
        // $access_code = ID (login do sistema) + ":" + codigo de acesso tudo em base64
	$retry = 0;
	denovo:
	$ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, ENDP_API);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

        $params = '{"numero": "' . $cartao . '"}';

        $headers = array();
        $headers[] = "Authorization: Basic " . $access_code;
        $headers[] = "accept: application/json";
        $headers[] = "Content-Type: application/json";
        $headers[] = "User-Agent: Letti (alfredo@letti.com.br)";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        $ret = curl_exec($ch);
        if (curl_errno($ch)) {
            Mage::log("correiosapi:getToken: Erro curl:" . curl_error($ch));
            curl_close ($ch);
	    $retry++;
	    if ($retry>4) return false;
	    goto denovo;
        }
        curl_close ($ch);
//var_dump($ret);
        if (strlen($ret) <= 0) {
            Mage::log("correiosapi:getToken: Erro: Cod acesso ou usr invalidos!");
	    $retry++;
	    if ($retry>4) return false;
	    goto denovo;
        }
        $json_str = json_decode($ret, true);
        $token = $json_str['token'];
        $expiraEm = $json_str['expiraEm'];
        $resp = array();
        $resp[] = $expiraEm;
        $resp[] = $token;
        Mage::log("correiosapi:getToken: pegou token, expiration=" . $expiraEm);
        return($resp);
}

function getPrazo($token, $servico, $cepOrigem, $cepDest)
{
	$ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, ENDP_API_PRAZO . $servico . "?cepOrigem=" . $cepOrigem  . "&cepDestino=" . $cepDest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $headers = array();
        $headers[] = "Authorization: Bearer " . $token;
        $headers[] = "accept: application/json";
        $headers[] = "User-Agent: Letti (alfredo@letti.com.br)";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $ret = curl_exec($ch);
        if (curl_errno($ch)) {
            Mage::log("correiosapi:getPrazo: Erro curl:" . curl_error($ch));
            return false;
        }
        curl_close ($ch);
        if (stristr($ret, "cesso não autorizado")) {
            Mage::log("correiosapi:getPrazo: Erro: " . $ret);
            return false;
        }
//var_dump($ret);
        $json_str = json_decode($ret, true);
        $prazo = $json_str['prazoEntrega'];
        $entregaDomiciliar = $json_str['entregaDomiciliar'];
        $entregaSabado = $json_str['entregaSabado'];
        $msgPrazo = "";
        if (isset($json_str['msgPrazo'])) $msgPrazo = $json_str['msgPrazo'];
        $saida = array();
        $saida[] = $prazo;
        $saida[] = $entregaDomiciliar;
        $saida[] = $entregaSabado;
        $saida[] = $msgPrazo;
        return($saida);
}

function getPreco($token, $servico, $cepOrigem, $cepDest, $peso, $tipoObj, $comprim, $largura, $altura, $servadc1, $valdeclarado)
{
	$ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, ENDP_API_PRECO . $servico . "?cepDestino=" . $cepDest  . "&cepOrigem=" . $cepOrigem . "&psObjeto=" . $peso . "&tpObjeto=" . $tipoObj . "&comprimento=" . $comprim . "&largura=" . $largura . "&altura=" . $altura . "&servicosAdicionais=" . $servadc1 . "&vlDeclarado=" . $valdeclarado);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $headers = array();
        $headers[] = "Authorization: Bearer " . $token;
        $headers[] = "accept: application/json";
        $headers[] = "User-Agent: Letti (alfredo@letti.com.br)";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $ret = curl_exec($ch);
        if (curl_errno($ch)) {
            Mage::log("correiosapi:getPrazo: Erro curl:" . curl_error($ch));
            return false;
        }
        curl_close ($ch);
        if (stristr($ret, "ERP-")) {
            Mage::log("correiosapi:getPreco: Erro: " . $ret);
            return false;
        }
        if (stristr($ret, "cesso não autorizado")) {
            Mage::log("correiosapi:getPreco: Erro: " . $ret);
            return false;
         }

        $json_str = json_decode($ret, true);
        $valor = $json_str['pcBaseGeral'];
        return($valor);
}

/*
Peso em gramas
servadc1 = servico adicional 1 = NULL
*/

function correiosapi($token, $servico, $cepOrigem, $cepDest, $peso, $tipoObj, $comprim, $largura, $altura, $servadc1, $valdeclarado)
{
// O token vem na chamada

//$servico = "04162"; // sedex

$prazo = getPrazo($token, $servico, $cepOrigem, $cepDest);
// retorna # dias  entregaDomiciliar  entregaSabado msgPrazo

//echo "prazo: " . print_r($prazo,true);

//$peso=100; 
//$tipoObj=1; 
//$comprim=20; 
//$largura=20; 
//$altura=20;
//$servadc1=""; 
//$valdeclarado=0; 

$valor = getPreco($token, $servico, $cepOrigem, $cepDest, $peso, $tipoObj, $comprim, $largura, $altura, $servadc1, $valdeclarado);
if (false === $valor) {
    Mage::log("correiosapi:erro ao obter preco!");
    return false;
}
//echo "preco = " . $valor;

$mxml = "<cServico>
<Codigo>XCODIGOX</Codigo>
<Valor>XVALORX</Valor>
<PrazoEntrega>XPRAZOX</PrazoEntrega>
<ValorSemAdicionais>XVALORX</ValorSemAdicionais>
<ValorMaoPropria>0,00</ValorMaoPropria>
<ValorAvisoRecebimento>0,00</ValorAvisoRecebimento>
<ValorValorDeclarado>0,00</ValorValorDeclarado>
<EntregaDomiciliar>XENTDOMX</EntregaDomiciliar>
<EntregaSabado>XENTSABADOX</EntregaSabado>
<obsFim>XMSGX</obsFim>
<Erro>XMSGNUMX</Erro>
<MsgErro>
<![CDATA[ XMSGX ]]>
</MsgErro>
</cServico>";

$mxml = str_replace("XCODIGOX", $servico, $mxml);
$mxml = str_replace("XVALORX", $valor, $mxml);
$mxml = str_replace("XPRAZOX", $prazo[0], $mxml);
$mxml = str_replace("XENTDOMX", $prazo[1], $mxml);
$mxml = str_replace("XENTSABADOX", $prazo[2], $mxml);
// sempre trocar o padrao por conteudo ou nada
$mxml = str_replace("XMSGX", $prazo[3], $mxml);
if ($prazo[3]) {
    $mxml = str_replace("XMSGNUMX", "011", $mxml);
} else $mxml = str_replace("XMSGNUMX", "0", $mxml);
return($mxml);
}
