# PedroTeixeira-Correios-Fix-API-Token

Após a mudança realizada na API dos Correios no dia 01/09/2023 tive problemas ao utilizar o módulo dos Correios desenvolvido por Pedro Teixeira. Busquei algumas soluções na internet e encontrei uma solução paleativa neste link: https://magentoblog.com.br/nova-apis-dos-correios-e-pedro-teixeira/. Lendo os comentários, percebi que diversas pessoas estão tendo dificuldades em implementar esta solução, então resolvi criar este repositório replicando as soluções que me ajudaram e colocando algum material extra para auxiliar a todos.

No repositório, está presente um documento chamado "1694698198674_novaapi.pdf" que é o arquivo que recebi dos correios, com o passo a passo para gerar as credenciais de acesso a nova API. Ps.: Os passos "e" e "f" são apenas para quem utiliza o sistema dos correios. Ou seja, paramos no passo "d" com o Código de Acesso Gerado.

Fiz também uma alteração no arquivo principal, para utilizar campos já presentes no Módulo do Pedro Teixeira e não precisar incluir dados no código fonte. O arquivo "campos-reutilizados.jpg" mostra quais campos estou usando.

    - Código do Cartão: É o código do cartão postagem que os correios enviam quando o contrato é feito com os Correios
    - ID dos correios: Gerado quando realizei o cadastro no site https://cws.correios.com.br/ conforme passo-a-passo do manual
    - Chave de acesso gerada: É a chave apresentada após a conclusão da etapa "d" do passo-a-passo